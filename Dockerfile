FROM alpine
ENV TERM screen-256color
RUN apk add --update neovim python3 git ncurses
ADD generate.vim /root/.config/nvim/init.vim
RUN apk add --update --virtual build-dependencies bash make curl gcc python3-dev musl-dev \
  && pip3 install pynvim \
  && nvim -i NONE -c PlugInstall -c quitall > /dev/null 2>&1 \
  && apk del build-dependencies \
  && infocmp $TERM | sed 's/kbs=^[hH]/kbs=\\177/' > /tmp/$TERM.ti \
  && tic /tmp/$TERM.ti
